
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

public class MatchController {

	P3_Thukral_Jasneet_MatchView view = new P3_Thukral_Jasneet_MatchView();
	P3_Thukral_Jasneet_MatchModel model = new P3_Thukral_Jasneet_MatchModel();

	// Random Images
	BufferedImage covered;
	BufferedImage blank;

	// Image ArrayList
	ArrayList<BufferedImage> Countries = new ArrayList<BufferedImage>();
	ArrayList<BufferedImage> Flags = new ArrayList<BufferedImage>();

	// Image Array
	Image[][] imgArr;

	public MatchController() {

	}

	public void addImagesToArrays() {
		// Upload the images
		try {

			for (int i = 1; i < (model.getNumRows() * model.getNumCols()) / 2 + 1; i++) {
				Countries.add(ImageIO.read(new File("c" + i + ".gif")));
				Flags.add(ImageIO.read(new File("f" + i + ".gif")));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void translateInnerArr() {
		for (int row = 0; row < model.getNumRows(); row++) {
			for (int col = 0; col < model.getNumCols(); col++) {
				if (model.getInsideInt(row, col) < 0) {
					System.out.println("neg");
					imgArr[row][col] = Flags.get(Math.abs(model.getInsideInt(row, col)));
				} else {
					System.out.println("pos");
					System.out.println(model.getInsideInt(row, col));
					System.out.println(Countries.size());
					imgArr[row][col] = Countries.get(model.getInsideInt(row, col));
				}
			}
		}
	}

	public void runGame() {
		askNumRC();
		model.initalizeInsideArr();
		setImgArrSize();
		model.printInArray();
		addImagesToArrays();
		translateInnerArr();
		view.setImageArr(imgArr);
		view.makeGui();
	}

	public void setImgArrSize() {
		imgArr = new Image[model.getNumRows()][model.getNumCols()];
	}

	public void askNumRC() {
		String numRC = JOptionPane.showInputDialog(null,
				"Eneter the number of rows and cols you desire: \n (type 4,6, or 8 for 4 by 4, 6 by 6, or 8 by 8 board)");
		while (Integer.parseInt(numRC) != 4 && Integer.parseInt(numRC) != 6 && Integer.parseInt(numRC) != 8) {
			numRC = JOptionPane.showInputDialog(null,
					"Eneter the number of rows and cols you desire: \n (type 4,6, or 8 for 4 by 4, 6 by 6, or 8 by 8 board)");
		}
		model.setNumRC(Integer.parseInt(numRC));
		System.out.println(Integer.parseInt(numRC));
	}

}
