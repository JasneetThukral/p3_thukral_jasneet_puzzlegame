
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Stroke;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class P3_Thukral_Jasneet_MatchView {

	// Image Array
	Image[][] imgArr;

	final int DRAWINGPANEL_WIDTH = 400;
	final int DRAWINGPANEL_HEIGHT = 400;

	// Attributes
	MyDrawingPanel drawingPanel;

	// listeners
	MouseListener clickListener;
	ActionListener newGameListener;

	//JFrame
	JFrame window;

	/******************************************************* Constructor *********************************************************************/
	public void makeGui() {

		// Create Java Window
		window = new JFrame("Matching Game!");
		window.setBounds(100, 100, 600, 600);
		window.setResizable(false);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		/**************************** Menu Stuff ***********************/

		/**************************** Layout ****************************/
		// Add all components to a JPanel called mainPanel
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new FlowLayout());

		// Grey Panel w game
		drawingPanel = new MyDrawingPanel();

		// When theres a click, translate the click position that can be read by
		// the array, pass into controller, which will pass into reveal method
		// Then when you repaint, it will automatically show the updates
		drawingPanel.addMouseListener(clickListener);
		drawingPanel.setPreferredSize(new Dimension(DRAWINGPANEL_WIDTH, DRAWINGPANEL_HEIGHT));
		drawingPanel.setBorder(BorderFactory.createEtchedBorder());

		// the grey thing that we will add the detector to
		mainPanel.add(drawingPanel);

		// Add the main panel to the content pane so we can see it
		window.getContentPane().add(mainPanel);

		// Let there be light
		window.setVisible(true);

	}

	/*******************************************************
	 * Inner Class
	 *********************************************************************/

	private class MyDrawingPanel extends JPanel {

		static final long serialVersionUID = 1234567890L;

		public void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;

			for (int row = 0; row < imgArr.length; row++) {
				for (int col = 0; col < imgArr[0].length; col++) {
					g2.drawImage(imgArr[row][col], col * (this.getWidth() / imgArr[0].length),
							row * (this.getHeight() / imgArr.length), this.getWidth() / 10, this.getHeight() / 10,
							null, null);

				}
			}

			// draw the lines
			Stroke str = new BasicStroke(3);
			g2.setStroke(str);
			g2.setColor(Color.black);
			for (int x = 0; x < this.getWidth(); x += this.getWidth()/imgArr[0].length)
				g2.drawLine(x, 0, x, this.getHeight());

			for (int y = 0; y < this.getHeight(); y += this.getHeight()/imgArr[0].length)
				g2.drawLine(0, y, this.getWidth(), y);

		}

	}

	/******************************************************* Methods *********************************************************************/

	public void setImageArr(Image[][] inputImgArr) {
		imgArr = new Image[inputImgArr.length][inputImgArr[0].length];
		for (int row = 0; row < inputImgArr.length; row++) {
			for (int col = 0; col < inputImgArr[0].length; col++) {
				imgArr[row][col] = inputImgArr[row][col];
			}
		}
	}

	public void paintAgain() {
		drawingPanel.repaint();
	}

}
