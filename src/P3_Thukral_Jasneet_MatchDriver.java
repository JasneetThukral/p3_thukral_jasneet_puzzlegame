import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/*Name: Jasneet Thukral 
 *Date: May 15, 2016
 *Period: 3
 *Time Spent: About 4 hours 
 *
 * About the Game: 
 * Rather than improving my previous game, I am working on creating a new game. I am creating a matching game, that involves two players. 
 * Players can choose to have a game board of 16, 36, or 64 tiles. Although right now the program only checks matches between a country 
 * and it's flag, I have made the program very flexible, so it can take in any pair of corresponding images and create a matching game 
 * based upon them. One of the most challenging parts of this creating this program was to effectively create all the images objects. At
 * first I tried to hard code all the images as seen in the code below:
 * 
 * //Country Images
	BufferedImage c1;
	BufferedImage c2;
	BufferedImage c3;
	....
	BufferedImage cn; (where n is an integer)
	
	However, I realized that this is very tedious, and not practical for uploading large quantities of images. Thus, I created a 
	for loop that looped through all the images and stored the flag images and "country name" images in an arraylist: 
	
	try {

			for (int i = 1; i < (model.getNumRows() * model.getNumCols()) / 2 + 1; i++) {
				Countries.add(ImageIO.read(new File("c" + i + ".gif")));
				Flags.add(ImageIO.read(new File("f" + i + ".gif")));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	I have tested my code for the matching game on the console using numbers (rather than images). However, I was not able to successfully 
	make it into a GUI. I keep getting a "javax.imageio.IIOException:". I have tried to understand how to fix this through 
	looking at stackOverflow, but I still do not understand why I am getting this error. 
 */

public class P3_Thukral_Jasneet_MatchDriver {
	public static void main(String[] args) {
		MatchController control = new MatchController(); 
		control.runGame();
	}
}
