
public class P3_Thukral_Jasneet_MatchIntFreq {

	int num;
	int freq;

	public P3_Thukral_Jasneet_MatchIntFreq(int num, int freq) {
		this.num = num;
		this.freq = freq;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getFreq() {
		return freq;
	}

	public void setFreq(int freq) {
		this.freq = freq;
	}
	
	@Override
	public String toString() {
		return "IntFreq [num=" + num + ", freq=" + freq + "]";
	}

	public void decreaseFreq(){
		this.freq -= 1; 
	}

}
