import java.util.ArrayList;

public class P3_Thukral_Jasneet_MatchModel {

	int NUM_ROWS;
	int NUM_COLS;

	int[][] outsideArr;
	int[][] insideArr;

	public void initalizeInsideArr() {

		// Initialize the outside array to all 0's
		for (int row = 0; row < outsideArr.length; row++) {
			for (int col = 0; col < outsideArr[0].length; col++) {
				outsideArr[row][col] = 0;
			}
		}

		// Initialize the inside array to random numbers
		// Ensure that 2 of each number are in the array

		// 1 - store a list of IntFreq objects
		ArrayList<P3_Thukral_Jasneet_MatchIntFreq> intFreqList = new ArrayList<P3_Thukral_Jasneet_MatchIntFreq>();
		for (int i = 0; i < (NUM_ROWS * NUM_COLS) / 2; i++) {
			P3_Thukral_Jasneet_MatchIntFreq intFreqObj = new P3_Thukral_Jasneet_MatchIntFreq(i + 1, 2);
			intFreqList.add(intFreqObj);
		}

		/*
		 * 2 - Loop through the array insideArr and in every index, add add a
		 * number from the intFreqList. Decrement the frequency by 1. Doing this
		 * will ensure that each number is used exactly twice (which is
		 * necessary for a matching game)
		 */

		for (int row = 0; row < insideArr.length; row++) {
			for (int col = 0; col < insideArr[0].length; col++) {
				int randomIndex = (int) (Math.random() * intFreqList.size());
				if (intFreqList.get(randomIndex).getFreq() == 2) {
					insideArr[row][col] = intFreqList.get(randomIndex).getNum();
				} else {
					insideArr[row][col] = intFreqList.get(randomIndex).getNum() * -1;
				}
				intFreqList.get(randomIndex).decreaseFreq();
				if (intFreqList.get(randomIndex).getFreq() == 0) {
					intFreqList.remove(randomIndex);
				}
			}
		}

	}

	public int getInsideInt(int row, int col) {
		return insideArr[row][col];
	}

	public int getNumRows() {
		return NUM_ROWS;
	}

	public int getNumCols() {
		return NUM_COLS;
	}

	public void setNumRC(int num) {
		NUM_ROWS = num;
		NUM_COLS = num;
		outsideArr = new int[NUM_ROWS][NUM_COLS];
		insideArr = new int[NUM_ROWS][NUM_COLS];
	}

	/* FOR TESTING PURPOSES */
	public void printInArray() {
		for (int row = 0; row < insideArr.length; row++) {
			for (int col = 0; col < insideArr[0].length; col++) {
				System.out.print(insideArr[row][col] + " ");
			}
			System.out.println();
		}
	}

	public void printOutArray() {
		for (int row = 0; row < outsideArr.length; row++) {
			for (int col = 0; col < outsideArr[0].length; col++) {
				System.out.print(outsideArr[row][col] + " ");
			}
			System.out.println();
		}
	}

}
